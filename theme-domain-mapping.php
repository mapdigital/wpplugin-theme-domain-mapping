<?php
/*
Plugin Name: Theme Domain Mapping
Plugin URI: https://bitbucket.org/mapdigital/wpplugin-theme-domain-mapping
Author: Map Digital
Author URI: http://mapdigital.com.au/
Description: Display your site differently depending on which domain it is being accessed from.
Version: 1.0
*/

$cls = 'ThemeDomainMapping';

// include the rest of the plugin framework

// Filter site domain to the one being accessed
add_filter('home_url', array($cls, 'alterSiteUrls'), 0, 4);
add_filter('site_url', array($cls, 'alterSiteUrls'), 0, 4);

// intercept template and stylesheet rendering with configured themes
if (!is_admin()) {
	add_filter('template', array($cls, 'handleTemplate'));
	add_filter('stylesheet', array($cls, 'handleStylesheet'));
}

// Setup robots correctly depending on domain visibility settings
add_filter('option_blog_public', array($cls, 'checkNoindexTheme'));

// Admin UI
add_action('admin_menu', array($cls, 'setupAdminScreens'));
add_action('load-appearance_page_' . ThemeDomainMapping::OPTIONS_PAGE_ID, array($cls, 'handleOptions'));
add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($cls, 'addPluginPageLinks'));
add_action('admin_enqueue_scripts', array($cls, 'enqueueAdminAssets'));

abstract class ThemeDomainMapping
{
	const OPTIONS_PAGE_PARENT = 'themes.php';
	const OPTIONS_PAGE_ID = 'theme-domain-mapping';

	const ASSET_VERSION = '1.0';	// admin script / styles version

	private static $options;
	private static $themes;

	//--------------------------------------------------------------------------
	// URL rewriting

	public static function alterSiteUrls($link, $path, $scheme, $blogId)
	{
		$domain = self::getRequestedDomain();
		$mainURL = get_option('siteurl');

		// don't do anything if we're on the active domain or we accessed via an invalid one
		if (!$domain || $domain == $mainURL) {
			return $link;
		}

		return preg_replace('@^' . preg_quote($mainURL, '@') . '@i', (is_ssl() ? 'https://' : 'http://') . $domain, $link);
	}

	private static function getRequestedDomain()
	{
		// check for CLI access
		if (!isset($_SERVER['HTTP_HOST'])) {
			return false;
		}

		$options = self::getOptions();
		$host = $_SERVER['HTTP_HOST'];

		$found = false;
		foreach ($options['mapped_domains'] as $theme => $domain) {
			if ($domain == $host) {
				$found = true;
				break;
			}
		}

		// check for invalid domain request
		if (!$found) {
			return false;
		}

		return $host;
	}

	//--------------------------------------------------------------------------
	// Template selection

	public static function handleTemplate($template)
	{
		$theme = self::getRequestedTemplate();
		$themes = self::getAvailableThemes();

		if (!$theme) {
			return $template;
		}

		$theme = $themes[$theme];

		// check for child theme and return parent's template if there is one
		if (!empty($theme['Template'])) {
			return $theme['Template'];
		}
		return isset($theme['Stylesheet']) ? $theme['Stylesheet'] : $template;
	}

	public static function handleStylesheet($stylesheet)
	{
		$theme = self::getRequestedTemplate();
		$themes = self::getAvailableThemes();

		if (!$theme) {
			return $stylesheet;
		}

		$theme = $themes[$theme];

		return isset($theme['Stylesheet']) ? $theme['Stylesheet'] : $stylesheet;
	}

	public static function shouldHandle($templateName)
	{
		$opts = self::getOptions();
		$domain = self::getRequestedDomain();

		if (!$domain || !isset($opts['mapped_domains'][$templateName])) {
			return false;
		}
		return true;
	}

	public static function getRequestedTemplate()
	{
		$opts = self::getOptions();
		$reqDomain = self::getRequestedDomain();

		foreach ($opts['mapped_domains'] as $theme => $domain) {
			if ($domain == $reqDomain) {
				return $theme;
			}
		}

		return null;
	}

	//--------------------------------------------------------------------------
	// Robots.txt handling for sortof-hidden domains

	private static $PROCESSING_ROBOTS = false;

	public static function checkNoindexTheme($public)
	{
		if (is_admin() || self::$PROCESSING_ROBOTS) {
			return $public;
		}

		$opts = self::getOptions();
		$handledTheme = self::getRequestedTemplate();

		if (!$handledTheme) {
			return $public;
		}

		$noIndex = isset($opts['noindex_domains'][$handledTheme]) && !$opts['noindex_domains'][$handledTheme];

		self::$PROCESSING_ROBOTS = true;
		$defaultOn = get_option('blog_public');
		self::$PROCESSING_ROBOTS = false;

		// remove some known plugin actions from appending to robots if it's inactive on the main site
		if (!$noIndex && !$defaultOn && class_exists('BWP_SIMPLE_GXS')) {
			global $bwp_gxs;
			remove_filter('robots_txt', array($bwp_gxs, 'do_robots'), 1000);
		}

		return $noIndex ? '0' : '1';
	}

	//--------------------------------------------------------------------------
	// Plugin options

	public static function getOptions()
	{
		if (!isset(self::$options)) {
			self::$options = array(
				'mapped_domains' => get_option('tdm_mapped_domains', array()),
				'noindex_domains' => get_option('tdm_noindex_domains', array()),
			);
		}
		return self::$options;
	}

	public static function getAvailableThemes()
	{
		if (isset(self::$themes)) {
			return self::$themes;
		}

		if (function_exists('wp_get_themes')) {
			self::$themes = wp_get_themes();
		} else {
			// :NOTE: backwards compatibility for Wordpress < 3.4
			self::$themes = get_themes();
		}

		return self::$themes;
	}

	//--------------------------------------------------------------------------
	//	Administration UI

	public static function setupAdminScreens()
	{
		add_submenu_page(self::OPTIONS_PAGE_PARENT,  __('Theme Domain Mapping'), __('Theme Domain Mapping'), 'manage_options', self::OPTIONS_PAGE_ID, array(get_class(), 'drawSettingsPage'));
	}

	public static function drawSettingsPage()
	{
		include('options-page.php');
	}

	public static function handleOptions()
	{
		if (!empty($_POST)) {
			$mappings = array();
			$indexOverrides = array();

			foreach ($_POST['mapped_domains'] as $theme => $domain) {
				$domain = preg_replace('@^https?://@i', '', trim($domain));
				$domain = preg_replace('@/.*$@i', '', $domain);
				if ($domain) {
					$mappings[$theme] = $domain;
				}
			}
			if (!empty($_POST['index_override'])) {
				foreach ($_POST['index_override'] as $theme => $override) {
					if ($override) {
						$indexOverrides[$theme] = !empty($_POST['index_on'][$theme]);
					}
				}
			}

			update_option('tdm_mapped_domains', $mappings);
			update_option('tdm_noindex_domains', $indexOverrides);

			add_action('admin_notices', array(get_class(), 'handleUpdateNotice'));
		}
	}

	public static function handleUpdateNotice()
	{
		echo '<div class="updated"><p>Settings saved. Please reload the page to update displayed values.</p></div>';
	}

	public static function enqueueAdminAssets()
	{
		global $pagenow;

		if ($pagenow == self::OPTIONS_PAGE_PARENT && $_REQUEST['page'] == self::OPTIONS_PAGE_ID) {
			wp_enqueue_style('tdm_admin_styles', plugins_url('res/admin-styles.css', __FILE__), false, self::ASSET_VERSION, 'all');
			wp_enqueue_script('tdm_admin_js', plugins_url('res/admin-script.js', __FILE__), array('jquery'), self::ASSET_VERSION, true);
		}
	}

	public static function addPluginPageLinks($links)
	{
		array_unshift($links, '<a href="themes.php?page=' . self::OPTIONS_PAGE_ID . '">Settings</a>');
		return $links;
	}
}
