(function($) {

// HELPERS

function toggleIndexOverride(checkbox)
{
	var $opts = checkbox.closest('.index-settings').find('.index-opts');

	if (checkbox.attr('checked')) {
		$opts.show();
	} else {
		$opts.hide();
	}
}

// DOMReady
$(function() {
	var indexCheckboxes = $('#theme-domain-mapping .index-settings :checkbox');

	indexCheckboxes.on('click', function(e) {
		toggleIndexOverride($(this));
	});

	indexCheckboxes.each(function() {
		toggleIndexOverride($(this));
	});
});

})(jQuery);
